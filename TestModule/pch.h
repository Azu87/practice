//
// pch.h
// Header for standard system include files.
//

#pragma once

#include "gtest/gtest.h"

#include <iostream>
#include <string>

using namespace std;

#define BEGIN_TEST(ClassName, Num) \
class ClassName##Num \
{ \
public: \
 ClassName##Num() { UnitTest(); } \
 \
virtual void UnitTest() \

#define END_TEST() \
};


