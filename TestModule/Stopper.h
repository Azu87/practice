#pragma once

#include <iostream>

class Stopper
{
public:
	~Stopper()
	{
		getchar();
	}
};