#pragma once

// 2. ���� �迭
template<typename T>
class MyVector
{
	const int nDEFAULT_COUNT = 10;
	const int nRESIZE = 5;
	T * m_arVal;

	unsigned int m_nSize;
	unsigned int m_nBufferSize;

public:

	MyVector()
	{
		m_nSize = 0;

		m_arVal = new T[nDEFAULT_COUNT];
		m_nBufferSize = 10;
	}

	~MyVector()
	{
		SAFE_DELETE_ARR(m_arVal);
	}

	void Clear()
	{
		m_nSize = 0;
	}

	int Size() const
	{
		return m_nSize;
	}

	int BufferSize() const
	{
		return m_nBufferSize;
	}

	T const * const GetBuffer() const
	{
		return m_arVal;
	}

	void Push(const T & a_Val)
	{
		if (m_nSize == m_nBufferSize)
		{
			int nMakeSize = m_nBufferSize + nRESIZE;
			T * pTemp = new T[nMakeSize];

			memcpy_s(pTemp, sizeof(T) * nMakeSize, m_arVal, sizeof(T) * m_nBufferSize);
			SAFE_DELETE_ARR(m_arVal);
			m_arVal = pTemp;
			m_nBufferSize = nMakeSize;
		}

		m_arVal[m_nSize] = a_Val;
		++m_nSize;
	}

	T & operator[](unsigned int a_nIndex) const
	{
		if (m_nSize <= a_nIndex)
		{
			assert(false && "error!");
		}

		return m_arVal[a_nIndex];
	}
};

// 3. mystring
class MyString
{
	MyVector<char>	m_vcVal;
	int				m_nSize;

private:

	void Add(char c)
	{
		if (m_vcVal.Size() <= m_nSize)
		{
			m_vcVal.Push(c);
		}
		else
		{
			m_vcVal[m_nSize] = c;
		}
	}

	void AddZero()
	{
		Add(0);
	}

public:

	MyString() { m_nSize = 0; }

	MyString(char a_cChar) : MyString()
	{
		operator+=(a_cChar);
	}

	MyString(const char * a_pS) : MyString()
	{
		operator+=(a_pS);
	}

	MyString(const MyString & a_pS) : MyString()
	{
		operator+=(a_pS);
	}

	void Clear()
	{
		m_nSize = 0;

		m_vcVal.Clear();
	}

	char const * const C_Str() const
	{
		return m_vcVal.GetBuffer();
	}

	int Size() const
	{
		return m_nSize;
	}

	char & operator[](unsigned int a_nIndex) const
	{
		if (m_nSize <= a_nIndex)
		{
			assert(false && "error!");
		}

		return m_vcVal[a_nIndex];
	}

	void operator=(char a_cChar)
	{
		Clear();
		operator+=(a_cChar);
	}

	void operator=(char * a_pS)
	{
		Clear();
		operator+=(a_pS);
	}

	void operator=(const MyString & a_refS)
	{
		Clear();
		operator+=(a_refS);
	}

	MyString operator+(char a_cChar)
	{
		MyString temp(*this);
		temp += a_cChar;
		return temp;
	}

	MyString operator+(char * a_pS)
	{
		MyString temp(*this);
		temp += a_pS;
		return temp;
	}

	MyString operator+(const MyString & a_refS)
	{
		MyString temp(*this);
		temp += a_refS;
		return temp;
	}

	void operator+=(char a_cChar)
	{
		Add(a_cChar);
		++m_nSize;

		AddZero();
	}

	void operator+=(const char * a_pStr)
	{
		if (a_pStr == nullptr) { return; }

		int nLen = strlen(a_pStr);

		for (int i = 0; i < nLen; ++i)
		{
			Add(a_pStr[i]);
			++m_nSize;
		}

		AddZero();
	}

	void operator+=(const MyString & a_refS)
	{
		int nLen = a_refS.Size();

		for (int i = 0; i < nLen; ++i)
		{
			Add(a_refS[i]);
			++m_nSize;
		}

		AddZero();
	}
};

// 4. stack
template <typename T>
class MyStack
{
public:

	const static int nMAX = 5;

private:

	T m_ar[nMAX];
	int m_nIndex;

public:

	MyStack()
	{
		memset(m_ar, 0, sizeof(T) * nMAX);
		m_nIndex = -1;
	}

	int Count()
	{
		return m_nIndex + 1;
	}

	void Push(const T & a_n)
	{
		if (Count() == nMAX) { return; }

		++m_nIndex;

		m_ar[m_nIndex] = a_n;
	}

	void Pop()
	{
		if (Count() <= 0) { return; }

		--m_nIndex;
	}

	const T & Peek()
	{
		if (Count() <= 0) { assert(false && "errro"); }

		return m_ar[m_nIndex];
	}
};

// 5. queue
template <typename T>
class MyQueue
{
public:

	const static int nMAX = 5;

private:

	T m_ar[nMAX + 1];

	int m_nFront;
	int m_nRear;

public:

	MyQueue()
	{
		memset(m_ar, 0, sizeof(T) * (nMAX + 1));
		m_nFront = m_nRear = 0;
	}

	int Count()
	{
		if (m_nRear == m_nFront) { return 0; }
		else if (m_nRear > m_nFront)
		{
			return m_nRear - m_nFront;
		}
		else
		{
			return m_nFront + (nMAX - m_nRear);
		}
	}

	void Push(const T & a_n)
	{
		if (Count() == nMAX) { return; }

		m_ar[m_nRear] = a_n;

		++m_nRear;

		if (m_nRear > nMAX)
		{
			m_nRear = 0;
		}
	}

	void Pop()
	{
		if (Count() <= 0) { return; }

		++m_nFront;

		if (m_nFront > nMAX)
		{
			m_nFront = 0;
		}
	}

	const T & Peek()
	{
		if (Count() <= 0) { assert(false && "errro"); }

		return m_ar[m_nFront];
	}
};
