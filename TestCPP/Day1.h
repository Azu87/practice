#pragma once

#include <iostream>

using uint = unsigned int;

uint Euclid(uint a, uint b)
{
	if (a == 0 || b == 0) { return 0; }

	while (b != 0)
	{
		if (a < b)
		{
			std::swap(a, b);
		}

		a = a - b;
	}

	return a;
}

uint EuclidMod(uint a, uint b)
{
	if (a == 0 || b == 0) { return 0; }

	if (a < b)
	{
		std::swap(a, b);
	}

	while (b > 1)
	{
		a %= b;
		std::swap(a, b);
	}

	return a;
}

uint EuclidNoStd(uint a, uint b)
{
	if (a == 0 || b == 0) { return 0; }

	uint t;

	while (b > 0)
	{
		if (b > a)
		{
			a = b;
		}

		t = a - b;
	}

	return t;
}

uint EuclidMod_NoStd(uint a, uint b)
{
	if (a == 0 || b == 0) { return 0; }

	uint t;

	while (b > 0)
	{
		t = a % b;
		a = b;
		b = t;
	}

	return a;
}
